package src;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import src.Node;
import src.Leaf;
import src.Recursive;

public class Main
{
	
	private static char[] specialChars = {'(', ')', '~', '^', 'v', '-', '<'};
	private static ArrayList<String> variables = new ArrayList<String>();

	public static void main(String[] args)
	{
		//printInfo();
		//String input = getInput();
		String input = "(AvB)^C";
		input = "(" + input + ")";
		//parseInput(input);
		//printstuffs();
		//System.out.println("You entered: " + input);
		
		//input = input.replaceAll("\\s", "");
		//thing = new Thingy();
		//thing.setText(input);
		
		parseInput3(input);
	}
	
	
	public static int parseInput3(String input)
	{
		input = input.replaceAll("\\s", "");
		char[] chars = input.toCharArray();
		
		
		int count = 0;
		for(int i = 0; i < chars.length; i++)
		{
			count += 1;
			if(chars[i] == '(')
			{
				// if '(', call self with stuff after the '(' as input
				parseInput3(input.substring(i + 1));
				i += count;
			}
			else if(true)//isOperator(chars[i], chars[i + 1]))
			{
				String var1 = input.substring(0, i);
				
				if(!variables.contains(var1))
				{
					variables.add(var1);
				}
				
				String var2 = input.substring(i + 1, input.indexOf(")"));
				
				if(!variables.contains(var2))
				{
					variables.add(var2);
				}
				
				System.out.println("Operator: " + chars[i]);
				
				
				// stuff to the left (to beginning?) is left operand
				// stuff to the right to closest ')' is right operand
				
				// add the operand to the variables list if it is not already in it
			}
			else if(chars[i] == ')')
			{
				return count;
				// go up a level: return
				// anything else to do before?
			}
		}
		
		return 1;
	}
	
	public static boolean isOperator(char char1, char char2)
	{
		switch(char1)
		{
			case '~':
			case '^':
			case 'v':return true;
					
			case '-':
			case '<':
			{
				if(char2 == '>')
				{
					return true;
				}
			}
			default:
			{
				return false;
			}
		}
	}
	
	
	
	
	private static void printstuffs()
	{
		for(int i = 0; i < variables.size(); i++)
		{
			System.out.println(variables.get(i));
		}
	}
	
	
	private static void parseInput2(String input)
	{
		int firstparen = input.indexOf("(");
		String reversed = new StringBuilder(input).reverse().toString();
		int lastparen = input.length() - reversed.indexOf(")") - 1; // subtract 1 to 0-index
		
		System.out.println(firstparen);
		System.out.println(lastparen);
		
		if(firstparen < 0)
		{
			// no opening paren
		}
		if(lastparen < 0)
		{
			// no closing paren
		}
		
		variables.add(input.substring(firstparen + 1, lastparen));
	}
	
	
	private static int parseInput(String input)
	{
		System.out.println("--------");
		int count = 0;
		for(char c : input.toCharArray())
		{
			count += 1;
			System.out.println(c);
			if(c == '(')
			{
				// start of a new block
				int newcount = parseInput(input.substring(input.indexOf("(") + 1));
				variables.add(input.substring(count, newcount));
				return count;
			}
			else if(c == ')')
			{
				// end of a block
				return count;
			}
			else if(c == '~')
			{
				// assign to node
			}
			else if(isOperator(c))
			{
				// assign nodes
			}
			else
			{
				// it is a A B C thingy
			}
		}
		
		return 0;
		
	}
	
	private static boolean isOperator(char c)
	{
		return false;
	}
	
	private static void printInfo()
	{
		System.out.println("------OVERVIEW------");
		System.out.println("This program takes user input in the form of a string of boolean arguments and connectors.");
		System.out.println("With this input, the program will determine if the boolean string is a tautology or not.");
		System.out.println("Proper operation assumes you have parenthasized your input correctly.");
		System.out.println("If you have not properly parenthasized your input, I hope you have a fire extinguisher ready for when this crashes and burns.");
		System.out.println("--------------------");
	}
	
	
	private static String getInput()
	{
		
		Scanner scanner = new Scanner(System.in);
		String input = "";
		
		System.out.print("Enter Something: ");
		
		input = scanner.nextLine();
		scanner.close();
		
		
		return input;
	}

}
