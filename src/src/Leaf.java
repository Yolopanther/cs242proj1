package src;

public class Leaf extends Node
{
	private String name;
	private boolean value;
	
	public Leaf()
	{
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setValue(boolean value)
	{
		this.value = value;
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean getValue()
	{
		return value;
	}
	
	@Override
	public boolean evaluate()
	{
		return value;
	}
}
