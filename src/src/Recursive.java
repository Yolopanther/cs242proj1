package src;

public class Recursive
{
	public static String[] recurse(int n)
	{
		int numrows = (int) Math.pow(2, n);
		String[] array = new String[numrows];
		
		if(n == 1)
		{
			array[0] = "1";
			array[1] = "0";
			return array;
		}
		
		String[] lesserarray = recurse(n-1);

		for(int i = 0; i < numrows; i++)
		{
			array[i] = lesserarray[i % (numrows / 2)]; // Copy lesser array twice such that the first and second halves of array are the same.
			array[i] += (i < numrows / 2) ? "1" : "0"; // Add 1 to end of each element of first half of array, 0 to second half.
		}
		
		return array;
	}
}
