package src;

public class Node
{
	Node parent;
	Node firstChild;
	Node secondChild;
	String operator;
	
	
	public Node()
	{
	}
	
	
	public void setOperator(String operator)
	{
		this.operator = operator;
	}
	
	
	public void setParent(Node node)
	{
		this.parent = node;
	}
	
	
	public void setFirstChild(Node node)
	{
		this.firstChild = node;
	}
	
	
	public void setSecondChild(Node node)
	{
		this.secondChild = node;
	}

	
	public boolean evaluate()
	{
		switch(operator)
		{
			case "~":
			{
				return !firstChild.evaluate();
			}
			case "^":
			{
				return (firstChild.evaluate() && secondChild.evaluate());
			}
			case "v":
			{
				return (firstChild.evaluate() || secondChild.evaluate());
			}
			case "->":
			{
				if((firstChild.evaluate() == true) && (secondChild.evaluate() == false))
				{
					return false;
				}
				return true;
			}
			case "<>":
			{
				if(((firstChild.evaluate() == true) && (secondChild.evaluate() == true)) || ((firstChild.evaluate() == false) && (secondChild.evaluate() == false)))
				{
					return true;
				}
				return false;
			}
			default:
			{
				throw new OperatorNotFoundException();
			}
		}
	}
}
