package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Main;

public class ParsingTest
{

	@Test
	public void isOperator()
	{
		assertTrue(Main.isOperator('~', '\0'));
		assertTrue(Main.isOperator('^', '\0'));
		assertTrue(Main.isOperator('v', '\0'));
		assertTrue(Main.isOperator('-', '>'));
		assertTrue(Main.isOperator('<', '>'));
		
		assertFalse(Main.isOperator('-', '<'));
		assertFalse(Main.isOperator('<', '<'));
		assertFalse(Main.isOperator('q', 'p'));
	}

}
