package tests;

import src.Node;
import src.Leaf;

public class TreeTest
{
	public static void main(String[] args)
	{
		System.out.println(test1());
		System.out.println(test2());
	}
	
	private static boolean test2()
	{
		// A v ~A
		
		boolean a = false;
		
		Node root = new Node();
		root.setOperator("v");
		
		Leaf var1 = new Leaf();
		var1.setParent(root);
		var1.setName("test1");
		var1.setValue(a);
		root.setFirstChild(var1);
		
		Node child1 = new Node();
		child1.setParent(root);
		child1.setOperator("~");
		root.setSecondChild(child1);
		
		Leaf var2 = new Leaf();
		var2.setParent(child1);
		child1.setFirstChild(var2);
		var2.setValue(a);
		
		return root.evaluate();
	}
	
	private static boolean test1()
	{
		// (A v B) ^ C
		
		boolean a = true;
		boolean b = true;
		boolean c = true;
		
		Node root = new Node();
		root.setOperator("^");
		
		Node child1 = new Node();
		child1.setOperator("v");
		child1.setParent(root);
		root.setFirstChild(child1);
		
		Leaf var1 = new Leaf();
		var1.setParent(child1);
		var1.setName("test1");
		var1.setValue(a);
		child1.setFirstChild(var1);
		
		Leaf var2 = new Leaf();
		var2.setParent(child1);
		var2.setName("test2");
		var2.setValue(b);
		child1.setSecondChild(var2);
		
		Leaf var3 = new Leaf();
		var3.setParent(root);
		var3.setName("test3");
		var3.setValue(c);
		root.setSecondChild(var3);
		
		return root.evaluate();
	}
	
}
